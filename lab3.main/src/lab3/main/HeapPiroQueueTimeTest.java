package lab3.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.hig.aod.lab3.BSTPriorityQueue;
import se.hig.aod.lab3.DuplicateItemException;
import se.hig.aod.lab3.EmptyQueueException;
import se.hig.aod.lab3.HeapPriorityQueue;

public class HeapPiroQueueTimeTest {

	public static void main(String[] args)
			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		HeapPiroQueueTimeTest test = new HeapPiroQueueTimeTest();

		test.heapNotSorted();
		System.out.println("*");
		test.heapSorted();
		System.out.println("**");
		test.heapSortedReversed();
		System.out.println("***");

		test.bstNotSorted();
		System.out.println("****");
		test.bstSorted();
		System.out.println("*****");
		test.bstSortedReversed();
		System.out.println("******");

	}

	public void heapNotSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData1 = loadListFromFile("data_640000.txt", 1000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		for (Integer number : newData1) {
			heap.enqueue(number);
		}

		List<Integer> newData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : newData2) {
			heap.enqueue(number);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();

		System.out.println("Tiden för *Heap enqueue* ej sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out.println("Tiden för *Heap dequeue* ej sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	public void heapSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> timeData1 = loadListFromFile("data_640000.txt", 1000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		Collections.sort(timeData1);
		for (Integer number : timeData1) {
			heap.enqueue(number);
		}

		List<Integer> timeData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : timeData2) {
			heap.enqueue(number);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		System.out.println("Tiden för *Heap enqueue* som sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out.println("Tiden för *Heap dequeue* som sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	public void heapSortedReversed()
			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> timeData1 = loadListFromFile("data_640000.txt", 1000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		Collections.sort(timeData1, Collections.reverseOrder());
		for (Integer number : timeData1) {
			heap.enqueue(number);
		}
		List<Integer> timeData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : timeData2) {
			heap.enqueue(number);
		}

		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		System.out.println(
				"Tiden för *Heap enqueue reserved* sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out
				.println("Tiden för *Heap dequeue reserved* sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	public void bstNotSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> timeData1 = loadListFromFile("data_640000.txt", 1000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();
		for (Integer number : timeData1) {
			bst.enqueue(number);
		}

		List<Integer> timeData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : timeData2) {
			bst.enqueue(number);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long lastTime = System.currentTimeMillis();

		System.out.println("Tiden för *BST enqueue* ej sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out.println("Tiden för *BST dequeue* ej sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	public void bstSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> timeData1 = loadListFromFile("data_640000.txt", 1000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();
		Collections.sort(timeData1);
		for (Integer number : timeData1) {
			bst.enqueue(number);
		}

		List<Integer> timeData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : timeData2) {
			bst.enqueue(number);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long lastTime = System.currentTimeMillis();

		System.out.println("Tiden för *BST enqueue* som sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out.println("Tiden för *BST dequeue* som sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	public void bstSortedReversed()
			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> timeData1 = loadListFromFile("data_640000.txt", 1000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();

		Collections.sort(timeData1, Collections.reverseOrder());
		for (Integer number : timeData1) {
			bst.enqueue(number);
		}
		List<Integer> timeData2 = loadListFromFile("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer number : timeData2) {
			bst.enqueue(number);
		}

		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		System.out
				.println("Tiden för *BST enqueue reserved* sorterad är: " + (timeAfter - timeBefore) + " milliseconds");
		System.out.println("Tiden för *BST dequeue reserved* sorterad är: " + (lastTime - timeAfter) + " milliseconds");
	}

	private List<Integer> loadListFromFile(String path, int size) throws FileNotFoundException, IOException {
		List<Integer> list = new ArrayList<Integer>();
		int cnt = 0;
		BufferedReader in = new BufferedReader(new FileReader(path));
		String l;
		while ((l = in.readLine()) != null && cnt < size) {
			list.add(Integer.parseInt(l));
			cnt++;
		}
		in.close();
		return list;
	}

	public boolean isFull() {
		// TODO Auto-generated method stub
		return false;
	}

}
