package lab3.main;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

//import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HeapPriorityQueueTest2 {

	static final int TEST_SIZE = 5;
	PriorityQueue<Integer> test;

	@BeforeEach
	void setUp() throws Exception {
		test = new HeapPriorityQueue<Integer>(TEST_SIZE);
		// test =new DummyQ<Integer>();
	}

	@AfterEach
	void tearDown() throws Exception {
		test = null;
	}

	@Test
	void isEmptyTest() {
		test.clear();
		assertTrue(test.isEmpty(), "Det är tomt!");

	}

	@Test
	void isFullTest() {

		assertTrue(test.isEmpty());
		assertFalse(test.isFull(), "Den bode vara falskt för tom kön");

		for (int i = 1; i <= TEST_SIZE; i++) {
			test.enqueue(i);
		}

		assertTrue(test.isFull(), "Det är fullt!");
	}

	@Test
	void clearTest() {
		assertTrue(test.isEmpty());

		test.enqueue(1);
		test.enqueue(2);
		assertFalse(test.isEmpty(), "Det är ej tomt!");
		test.clear();
		assertTrue(test.isEmpty(), "Det är tomt!");
	}

	@Test
	void enqueueTest() {
		test.enqueue(1);
		test.enqueue(2);
		assertEquals(test.dequeue(), 1);
	}

	@Test
	void dequeueTest() {
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.dequeue(), 1);
	}

	@Test
	void getFrontTest() {
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.getFront(), 1);
		test.dequeue();
		assertEquals(test.getFront(), 5);

		// System.out.print(test.getFront());
	}

	@Test
	void size() {
		assertEquals(test.size(), 0);
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.size(), 3);
		test.dequeue();
		assertEquals(test.size(), 2);
		test.clear();
		assertEquals(test.size(), 0);
	}

	@Test
	void diffrentsFelTest() {

		assertThrows(RuntimeException.class, () -> test.getFront());

		assertThrows(RuntimeException.class, () -> test.dequeue());

		test.enqueue(1);
		assertDoesNotThrow(() -> test.getFront()); // Nytt, prova igen själv
		test.enqueue(2);
		test.enqueue(3);
		test.enqueue(4);
		test.enqueue(5);

		assertThrows(RuntimeException.class, () -> test.enqueue(6));

		assertThrows(RuntimeException.class, () -> test = new HeapPriorityQueue<Integer>(-2));
	}

}