package lab3.main;

public interface PriorityQueue <T extends Comparable<? super T>> {
	
	  /** 
     * Töm prioritetskön (töm den på element och återlämna minnet) 
     */
    public void clear();
    
    /**
     * Undersök om prioritetskö är tom.
     */
    public boolean isEmpty();
    
    /**
     * Undersök om prioritetskö är full. 
     */
    public boolean isFull();
    
    /**
     * Returnera antalet element i prioritetskön
     * 
     */
   
    public int size();
    
    /**
     * Sätt in ett element i prioritetskön 
     * (i prioritetsordning efter Comparable)
     */
    
    public void enqueue(T t);
    
    /**
     *Ta bort första elementet (det element som har högst prioritet) 
     *från prioritetskön och returnera
     *det till användaren
     */
   public T dequeue();
    
   /**
    * Titta på första elementet (det element som har högst prioritet) 
    * utan att ta bort det
    */
    public T getFront();

}
