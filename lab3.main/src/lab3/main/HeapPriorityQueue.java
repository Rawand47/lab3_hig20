package lab3.main;

public class HeapPriorityQueue<T extends Comparable<T>> implements PriorityQueue<T> {

	private int size;
	private int rear;
	private Object[] array;
	private int currSize;

	public HeapPriorityQueue(int size) {
		if (size > 0) {
			rear = -1;
			this.size = size;
			array = new Object[size];
			this.currSize = 0;
		} else
			throw new RuntimeException("Storleken bör vara > *0*");
	}

	@Override
	public void clear() {
		currSize = 0;
	}

	@Override
	public boolean isEmpty() {
		if (currSize == 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean isFull() {
		if (currSize == array.length) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public int size() {
		return currSize;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void enqueue(Comparable t) {

		if (isFull()) {
			throw new RuntimeException("Kan inte göra enqueue!");
		}

		else {
			rear = (rear + 1);
			array[rear] = (T) t;
			currSize++;
			StigandeHeap();
		}
	}

	@SuppressWarnings("unchecked")
	private void StigandeHeap() {

		int jumpUpElement = rear;
		while (jumpUpElement > 0) {
			int jumpUpelement = (jumpUpElement - 1) / 2;

			T object = (T) array[jumpUpElement];

			T parent = (T) array[jumpUpelement];

			if (object.compareTo(parent) < 0) {

				array[jumpUpElement] = parent;
				array[jumpUpelement] = object;
				jumpUpElement = jumpUpelement;

			} else
				return;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T dequeue() {

		T dequeueLow = null;
		if (isEmpty()) {
			throw new RuntimeException("Kan inte göra Dequeue!");
		}
		if (currSize == 1) {

			Object tempArray = array[0];
			array[0] = null;
			rear--;
			currSize--;
			return (T) tempArray;

		} else {
			dequeueLow = (T) array[0];

			array[0] = array[rear];
			rear--;
			currSize--;
			fallandeHeap();
		}
		return dequeueLow;
	}

	@SuppressWarnings("unchecked")
	private void fallandeHeap() {

		int saveValue = 0;
		int childLeft = 2 * saveValue + 1;

		while (childLeft < currSize) {
			int smallestElement = childLeft;
			int childRight = childLeft + 1;
			if (childRight < currSize) {
				if (((Comparable<T>) array[childRight]).compareTo((T) array[childLeft]) < 0) {
					smallestElement++;
				}
			}

			if (((Comparable<T>) array[saveValue]).compareTo((T) array[smallestElement]) > 0) {

				T temp = (T) array[saveValue];
				array[saveValue] = array[smallestElement];
				array[smallestElement] = temp;
				saveValue = smallestElement;
				childLeft = 2 * saveValue + 1;

			} else
				return;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getFront() {

		if (isEmpty()) {
			throw new RuntimeException("Det är tomt!");
		} else {
			return (T) array[0];
		}
	}

}
